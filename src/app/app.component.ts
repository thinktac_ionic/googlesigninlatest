import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
// import { AngularFireAuth } from '@angular/fire/auth';
// import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    // public afAuth: AngularFireAuth,
    // public router: Router,
  ) {
    this.initializeApp();

  }
  ngOnInit() {
    // this.afAuth.authState.subscribe(user => {
    //   if (user) {
    //     this.router.navigate(["/profile"]);
    //   } else {
    //     this.router.navigate(["/login"]);
    //   }
    // });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
    });
  }
}
