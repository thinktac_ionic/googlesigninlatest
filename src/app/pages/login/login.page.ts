import { Component, OnInit } from '@angular/core';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Platform, LoadingController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { environment } from 'src/environments/environment';
import * as firebase from 'firebase';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user$: Observable<any>;

  constructor(private afAuth: AngularFireAuth,
    private googlePlus: GooglePlus,
    private platform: Platform,
    private router: Router,
    private afs: AngularFirestore,
    public loadingController: LoadingController
  ) {
    // this.user$ = this.afAuth.authState.pipe(
    //   switchMap(user => {
    //     if (user) {
    //       return this.afs.doc<firebase.User>(`users/${user.uid}`).valueChanges();
    //     } else {
    //       return of(null);
    //     }
    //   })
    // );
  }

  async ionViewWillEnter() {
    const loading = await this.loadingController.create({
      message: 'Please wait...'
    });
    this.presentLoading(loading);

    if (this.afAuth.auth.currentUser) {
      this.router.navigate(["/profile"]);
    }

    loading.dismiss();
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  ngOnInit() {

  }

  googleLogin() {
    if (this.platform.is('cordova')) {
      this.nativeGoogleLogin();
    } else {
      this.webGoogleLogin();
    }
  }
  async nativeGoogleLogin(): Promise<void> {
    try {
      const user = await this.googlePlus.login({
        'scopes': '', // optional - space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
        'webClientId': environment.googleWebClientId, // optional - clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
        'offline': true, // Optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
      })
      const firebaseUser = await this.afAuth.auth.signInWithCredential(firebase.auth.GoogleAuthProvider.credential(user.idToken));
      this.updateUserData(firebaseUser);
      // this.router.navigate(["/profile"]);
    } catch (err) {
      console.log(err)
    }
  }

  async webGoogleLogin(): Promise<void> {
    try {
      const provider = new firebase.auth.GoogleAuthProvider();
      const authUser = await this.afAuth.auth.signInWithPopup(provider);
      const userDetails = {
        uid: authUser.user.uid,
        displayName: authUser.user.displayName,
        email: authUser.user.email,
        photoURL: authUser.user.photoURL,
      }
      this.updateUserData(userDetails);
    } catch (err) {
      console.log(err)
    }
  }

  private async updateUserData(firebaseUser) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`Employees/${firebaseUser.email}`);
    const data = {
      uid: firebaseUser.uid,
      email: firebaseUser.email,
      displayName: firebaseUser.displayName,
      photoURL: firebaseUser.photoURL
    }
    this.router.navigate(["/profile"]);
    return userRef.set(data, { merge: true })
  }
}


