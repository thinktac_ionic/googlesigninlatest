import { Component, OnInit } from '@angular/core';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  user: any;
  name: string;
  email: string;
  image: string;

  constructor(private afAuth: AngularFireAuth,
    private googlePlus: GooglePlus,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.user = this.afAuth.auth.currentUser
    this.name = this.user.displayName;
    this.email = this.user.email;
    this.image = this.user.photoURL;
  }

  signOut() {
    this.googlePlus.logout();
    this.afAuth.auth.signOut();
    this.router.navigate(["/login"]);
  }
}
